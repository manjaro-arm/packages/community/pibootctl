# Author: Dave Jones <dave@waveform.org.uk>

pkgname=pibootctl
url="https://github.com/waveform80/${pkgname}"
pkgver=0.6
pkgrel=3
provides=($pkgname)
pkgdesc="Utility for querying and manipulating the Raspberry Pi boot configuration"
arch=('aarch64')
license=('GPL3')
depends=('python>=3' 'python-pkginfo' 'python-yaml')
makedepends=('python-sphinx' 'python-setuptools')
source=("https://github.com/waveform80/${pkgname}/archive/refs/tags/v${pkgver}.tar.gz"
        'pibootctl.conf')

md5sums=('501ae15380979775b654a89dbabdcf17'
         '218bfb140175089c4f6d74f740963a61')

build()
{
  # build man pages
  cd $pkgname-$pkgver/docs
  make man
  cd ../build/man
  gzip *
}

package()
{
  cd $pkgname-$pkgver
  mkdir -p $pkgdir/etc/
  mkdir -p $pkgdir/usr/share/man/man1
  mkdir -p $pkgdir/usr/share/licenses/$pkgname
  install -Dm 0644 ../$pkgname.conf $pkgdir/etc/
  install -Dm 0644 LICENSE.txt $pkgdir/usr/share/licenses/$pkgname/
  install -Dm 0644 docs/license.rst $pkgdir/usr/share/licenses/$pkgname/
  install -Dm 0644 build/man/*.gz $pkgdir/usr/share/man/man1/
  python setup.py install --prefix=usr --root="${pkgdir}"
}
